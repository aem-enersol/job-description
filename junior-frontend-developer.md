[![AEM Enersol](http://i0.wp.com/aemenersol.com/wp-content/uploads/2015/12/Logo-AEM-for-MegaProject-Final.png?fit=290%2C129)](http://aemenersol.com)

# Who We Are

AEM Enersol is an independent integrated consultancy services, from upstream to downstream. Our impartiality allows us to provide a high quality advise to optimize clients' portfolio in a business. Our principle is grounded in an ultimate priority - achieving clients' needs at beyond the best limit.

# About the Position

We are looking for an ***Junior Angular 2+ Frontend Developer*** responsible for the client side of our product. Your primary focus will be to implement a complete user interface in the form of a web app, with a focus on performance. You will work in a team with the back-end developer, and communicate with the API using standard methods.

### Responsibilities
- Use markup languages like HTML to create user-friendly web pages
- Maintain and improve existing web application
- Collaborate with back-end developers and web designers to improve usability
- Get feedback from, and build solutions for, users and customers
- Stay up-to-date on emerging technologies

### Technical Requirements
- Minimum 3 years working experience.
- Experience in developing application with Angular 2+.
- Experience in designing UI with CSS, HTML5, Saas, Mixins, Less.
- Knowledge in using frontend package management and testing tools. eg : Yarn, Npm, Babel, Selenium.
- Experience in integrating and consuming webservice eg: REST/SOAP.
- Experience in using distributed version control systems. eg: git, mercurial.

### Benefit
- Write benefit here